def num_to_s(num, base)
  num_hash = {0 => "0",
              1 => "1",
              2 => "2",
              3 => "3",
              4 => "4",
              5 => "5",
              6 => "6",
              7 => "7",
              8 => "8",
              9 => "9",
              10 => "A",
              11 => "B",
              12 => "C",
              13 => "D",
              14 => "E",
              15 => "F" }
  strings =("1".."9").to_a
  letters = ("A".."F").to_a
  nums = (1..9).to_a

  result = ""
  magnitude = 1
  while magnitude <= num
    result += num_hash[(num / magnitude) % base]
    magnitude *= base
  end

  result.reverse
end


def caesar(string,shift)
  string.split(" ").map { |word| cipher_word(word,shift) }.join(" ")
end

def cipher_word(word,shift)
  result = ""
  word.each_char do |char|
    asc_num = char.ord + shift
    if asc_num < 'z'.ord
      result += asc_num.chr
    else
      result += (((asc_num - 97) % 26) + 97).chr
    end
  end

  result
end















