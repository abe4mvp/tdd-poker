# -*- coding: utf-8 -*-

# Represents a deck of playing cards.
class Deck
  # Returns an array of all 52 playing cards.
  def self.all_cards
    cards = []
    Card.suits.each do |suit|
      Card.values.each do |value|
        cards << Card.new(suit,value)
      end
    end

    cards
  end

  def initialize(cards = Deck.all_cards)
    @cards = cards
  end

  # Returns the number of cards in the deck.
  def count
    cards.size
  end

  def include?(card)
    cards.include?(card)
  end

  def peek
    cards.first
  end

  def shuffle
    cards.shuffle!
  end

  # Takes `n` cards from the top of the deck.
  def take(n)
    if cards.size >= n
      cards.shift(n)
    else
      raise "not enough cards"
    end
  end

  # Returns an array of cards to the bottom of the deck.
  def return(return_cards)
    cards.push(*return_cards)
  end
  private
  attr_accessor :cards
end



# Represents a playing card.
class Card
  SUIT_STRINGS = {
    :clubs    => "♣",
    :diamonds => "♦",
    :hearts   => "♥",
    :spades   => "♠"
  }

  VALUE_STRINGS = {
    :deuce => "2",
    :three => "3",
    :four  => "4",
    :five  => "5",
    :six   => "6",
    :seven => "7",
    :eight => "8",
    :nine  => "9",
    :ten   => "10",
    :jack  => "11",
    :queen => "12",
    :king  => "13",
    :ace   => "14"
  }

  CARD_RANK = {
    :deuce => 2,
    :three => 3,
    :four  => 4,
    :five  => 5,
    :six   => 6,
    :seven => 7,
    :eight => 8,
    :nine  => 9,
    :ten   => 10,
    :jack  => 11,
    :queen => 12,
    :king  => 13,
    :ace => 14
  }

  # Returns an array of all suits.
  def self.suits
    SUIT_STRINGS.keys
  end

  # Returns an array of all values.
  def self.values
    VALUE_STRINGS.keys
  end

  def card_rank
    CARD_RANK[:value]
  end

  attr_reader :suit, :value

  def initialize(suit, value)
    unless Card.suits.include?(suit) and Card.values.include?(value)
      raise "illegal suit (#{suit.inspect}) or value (#{value.inspect})"
    end

    @suit, @value = suit, value
  end




  # Compares two cards to see if they're equal in suit & value.
  def ==(other_card)
    (self.suit == other_card.suit) && (self.value == other_card.value)
  end

  def to_s
    VALUE_STRINGS[value] + SUIT_STRINGS[suit]
  end
end
