class Array
  def my_uniq
    seen = []
    self.each { |el| seen << el unless seen.include?(el) }
    seen
  end

  def two_sum
    result = []
    i = 0
    while i < self.length - 1
      j = i + 1
      while j < self.length
        if self[i] + self[j] == 0
          result << [i, j]
        end
        j+=1
      end
      i+=1
    end

    result
  end

  def my_transpose
    result = Array.new(self.size) {Array.new(self.size)}
    self.each_with_index do |row,row_num|
      row.each_with_index do |el,col_num|
        result[col_num][row_num] = el
      end
    end

    result
  end

  def stock_picker
    days = []
    best = 0
    i = 0
    while i < self.size - 1
      j = i + 1
      while j < self.size
        if self[j] - self[i] > best
          best = self[j] - self[i]
          days = [i,j]
        end
        j += 1
      end
      i += 1
    end

    days
  end


end


class Tower
  attr_accessor :piles

  def initialize
    @piles = [[4,3,2,1],[],[]]
  end

  def render
    piles.to_s
  end

  def move(move_array)
    disc = piles[move_array.first].pop
    if piles[move_array.last].empty? || disc < piles[move_array.last].last
      piles[move_array.last] << disc
    else
      raise "invalid move"
    end

  end

  def won?
    piles[1].size == 4 || piles[2].size == 4
  end

end