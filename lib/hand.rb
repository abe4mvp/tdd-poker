class Hand
  # This is called a *factory method*; it's a *class method* that
  # takes the a `Deck` and creates and returning a `Hand`
  # object. This is in contrast to the `#initialize` method that
  # expects an `Array` of cards to hold.
  def self.deal_from(deck)
    Hand.new(deck.take(5))
  end

  attr_accessor :cards

  def initialize(cards)
    @cards = cards
  end

  def points

  end

  def busted?
  end

  def hit(deck)
  end

  def beats?(other_hand)
    hierarchy = [:royal_flush?, :straight_flush?, :four_of?, :full?, :flush?,
      :straight?, :three_of?, :two_pair?, :pair?, :high_card?]
  end


  def royal_flush?
  end

  def straight_flush?
  end

  def four_of?
  end

  def full?
  end

  def flush?
  end

  def straight?
    # in_order = [:ace, :deuce, :three,
#       :four, :five, :six, :seven, :eight,
#       :nine, :ten, :jack, :queen, :king, :ace]
    card_hand = []
    cards.each do |card|
      card_hand << card.card_rank
    end
    p card_hand
    ace_low = card_hand.map {|card| card == 14 ? 1 : card}
    consecutive?(card_hand) || consecutive?(ace_low)
  end

  def consecutive?(arr)
    ((arr.max - arr.min) == 4) && arr.uniq.size == 5
  end


  def three_of?
    uniq_cards = Hash.new(0)
    cards.each do |card|
      uniq_cards[card.value] += 1
    end
    three = false
    uniq_cards.each_value do |count|
      three = true if count == 3
    end
    three
  end

  def two_pair?
    uniq_cards = []
    cards.each do |card|
      uniq_cards << card.value unless uniq_cards.include?(card.value)
    end
    uniq_cards.size == 3
  end

  def pair?
    uniq_cards = []
    cards.each do |card|
      uniq_cards << card.value unless uniq_cards.include?(card.value)
    end
    uniq_cards.size == 4
  end





  # def high_card_value
  #   best_card = 0
  #   cards.each do |card|
  #     val = Card.CARD_RANK[card.value]
  #     best_card
  #   end
  # end




  def royal_flush

  end


  def return_cards(deck)
  end

  def to_s
    @cards.join(",") + " (#{points})"
  end
end
