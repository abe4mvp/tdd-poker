require 'rspec'
# require 'card.rb'
# require 'deck.rb'
require 'hand.rb'

describe Hand do
  let(:card1) { double(:card1) }
  let(:card2) { double(:card2) }
  let(:card3) { double(:card3) }
  let(:card4) { double(:card4) }
  let(:card5) { double(:card5) }

  describe "#two_pair?" do

    it "correctly identified a two pair hand" do

      card1.stub(:value).and_return(:three)
      card2.stub(:value).and_return(:three)
      card3.stub(:value).and_return(:four)
      card4.stub(:value).and_return(:five)
      card5.stub(:value).and_return(:five)

      cards = [card1, card2, card3, card4, card5]

      expect(Hand.new(cards).two_pair?).to eq(true)
    end


    it "correctly identified a non-two pair hand" do

      card1.stub(:value).and_return(:seven)
      card2.stub(:value).and_return(:three)
      card3.stub(:value).and_return(:four)
      card4.stub(:value).and_return(:five)
      card5.stub(:value).and_return(:five)

      cards = [card1, card2, card3, card4, card5]

      expect(Hand.new(cards).two_pair?).to eq(false)
    end


  end

  describe "#three_of?" do
    it "correctly identified a three of a kind" do

      card1.stub(:value).and_return(:three)
      card2.stub(:value).and_return(:deuce)
      card3.stub(:value).and_return(:five)
      card4.stub(:value).and_return(:five)
      card5.stub(:value).and_return(:five)

      cards = [card1, card2, card3, card4, card5]

      expect(Hand.new(cards).three_of?).to eq(true)
    end


    it "correctly identified a non-three of a kind" do

      card1.stub(:value).and_return(:seven)
      card2.stub(:value).and_return(:three)
      card3.stub(:value).and_return(:four)
      card4.stub(:value).and_return(:five)
      card5.stub(:value).and_return(:five)

      cards = [card1, card2, card3, card4, card5]

      expect(Hand.new(cards).three_of?).to eq(false)
    end

    it "correctly identified a non-three of a kind (extra)" do

      card1.stub(:value).and_return(:seven)
      card2.stub(:value).and_return(:three)
      card3.stub(:value).and_return(:four)
      card4.stub(:value).and_return(:five)
      card5.stub(:value).and_return(:five)

      cards = [card1, card2, card3, card4, card5]

      expect(Hand.new(cards).three_of?).to eq(false)
    end


  end


  describe "#straight?" do
    it "correctly identified a straight (ace low)" do

      card1.stub(:card_rank).and_return(3)
      card2.stub(:card_rank).and_return(2)
      card3.stub(:card_rank).and_return(4)
      card4.stub(:card_rank).and_return(5)
      card5.stub(:card_rank).and_return(14)

      cards = [card1, card2, card3, card4, card5]

      expect(Hand.new(cards).straight?).to eq(true)
    end

    it "correctly identified a straight (ace high)" do

      card1.stub(:card_rank).and_return(11)
      card2.stub(:card_rank).and_return(10)
      card3.stub(:card_rank).and_return(12)
      card4.stub(:card_rank).and_return(13)
      card5.stub(:card_rank).and_return(14)

      cards = [card1, card2, card3, card4, card5]

      expect(Hand.new(cards).straight?).to eq(true)
    end


    it "correctly identified a non-straight" do

      card1.stub(:card_rank).and_return(3)
      card2.stub(:card_rank).and_return(2)
      card3.stub(:card_rank).and_return(4)
      card4.stub(:card_rank).and_return(5)
      card5.stub(:card_rank).and_return(7)

      cards = [card1, card2, card3, card4, card5]

      expect(Hand.new(cards).straight?).to eq(false)
    end

    it "correctly identified a non-straight (extra)" do

      card1.stub(:card_rank).and_return(3)
      card2.stub(:card_rank).and_return(2)
      card3.stub(:card_rank).and_return(11)
      card4.stub(:card_rank).and_return(5)
      card5.stub(:card_rank).and_return(10)

      cards = [card1, card2, card3, card4, card5]

      expect(Hand.new(cards).straight?).to eq(false)
    end


  end



end
