require 'rspec'
require '00_array.rb'


describe "#my_uniq" do
  it "returns an array" do
    [1, 2, 1, 3, 3].my_uniq.should be_instance_of(Array)
  end

  it "returns empty array" do
    [].my_uniq.should == []
  end

  it "removes duplicates" do
    [1, 2, 1, 3, 3].my_uniq.should == [1, 2, 3]
  end

end

describe "#two_sum" do
  it "returns Array" do
    [].two_sum.should be_instance_of(Array)
  end

  it "returns empty array" do
    [].my_uniq.should == []
  end

  it "returns a nested array" do
    [-1, 1].two_sum[0].should be_instance_of(Array)
  end

  it "handles simple array" do
    [-2, -1, 1, 2].two_sum.should == [[0, 3], [1, 2]]
  end

  it "handles multiple arrays" do
    [-1, 0, 2, -2, 1].two_sum.should == [[0, 4], [2, 3]]
  end

  it "handles repeated numbers" do
    [-1, 0, 2, -2, 1, 1].two_sum.should == [[0, 4], [0,5], [2, 3]]
  end

end


describe "#my_transpose" do
  row = [ [0, 1, 2],
           [3, 4, 5],
           [6, 7, 8] ]

  cols = [ [0, 3, 6],
           [1, 4, 7],
           [2, 5, 8] ]

  it "returns Array" do
    row.my_transpose.should be_instance_of(Array)
  end

  it "maintains 2D array" do
    row.my_transpose[0].should be_instance_of(Array)
  end

  it "transposes properly" do
    row.my_transpose.should == cols
  end

end

describe "#stocker_picker" do

  it "returns an array" do
    [1,3,15].stock_picker.should be_instance_of(Array)
  end

  it "solves easy case" do
    [1,3,15].stock_picker.should == [0,2]
  end

  it "solves medium case" do
    [3,1,15].stock_picker.should == [1,2]
  end

  it "solves hard case" do
    [5,34,1,45,45,49].stock_picker.should == [2,5]
  end


end


describe Tower do
  subject(:tower) { Tower.new }
  describe "#render" do
    it "returns proper format" do
      tower.render.should be_an_instance_of(String)
    end

    it "shows default value" do
      tower.render.should == "[[4, 3, 2, 1], [], []]"
    end
  end

  describe "#move" do
    it "moves a disc" do
      tower.move([0,1])
      tower.piles.should == [[4, 3, 2], [1], []]
    end

    it "returns an error if invalid move" do
      expect do
        tower.move([0,1])
        tower.move([0,1])
      end.to raise_error("invalid move")
    end
  end

  describe "#won?" do
    it "should identify winning condition" do
        tower.piles = [[],[],[4,3,2,1]]
        tower.won?.should == true
    end
  end

end





#
#
#   describe "move methods" do
#     it "moves left" do
#       robot.move_left
#       robot.position.should eq([-1, 0])
#     end
#
#     it "moves right" do
#       robot.move_right
#       robot.position.should eq([1, 0])
#     end
#
#     it "moves up" do
#       robot.move_up
#       robot.position.should eq([0, 1])
#     end
#
#     it "moves down" do
#       robot.move_down
#       robot.position.should eq([0, -1])
#     end
#   end
# end