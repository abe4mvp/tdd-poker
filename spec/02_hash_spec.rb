require 'rspec'
require '02_hash'


subject(:hash_inst) { Hash.new }

  describe "#num_to_s" do
    it "takes decimal" do
      num_to_s(5, 10).should == "5"
      num_to_s(234, 10).should ==  "234"
    end

    it "takes binary" do
      num_to_s(5, 2).should ==  "101"
      num_to_s(234, 2).should ==  "11101010"
    end

    it "takes hexadecimal" do
      num_to_s(5, 16).should ==  "5"
      num_to_s(234, 16).should ==  "EA"
    end

    it "takes hexadecimal" do
      hash_inst.set_add_el({}, :x).should == {:x => true}
    end
    set_add_el({:x => true}, :x).should == {:x => true}
    set_remove_el({:x => true}, :x).should == {}
    set_list_els({:x => true, :y => true}).should == [:x, :y]
    set_member?({:x => true}, :x).should == true
    set_union({:x => true}, {:y => true}).should == {:x => true, :y => true}
    set_intersection.should
    set_minus # Return all elements of the first hash that are not in the second hash, not vice versa



  end

