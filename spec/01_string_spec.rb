require 'rspec'
require '01_string.rb'


describe "#num_to_s" do
  it "takes decimal" do
    num_to_s(5, 10).should == "5"
    num_to_s(234, 10).should ==  "234"
  end

  it "takes binary" do
    num_to_s(5, 2).should ==  "101"
    num_to_s(234, 2).should ==  "11101010"
  end

  it "takes hexadecimal" do
    num_to_s(5, 16).should ==  "5"
    num_to_s(234, 16).should ==  "EA"
  end

end


describe "#caesar_cipher" do


  it "does single letter" do
    caesar("a", 5).should == "f"
  end

  it "does two letters" do
    caesar("ab", 1).should == "bc"
  end

  it "does 'hello'" do
    caesar("hello", 3).should == "khoor"
  end

  it "wraps around z" do
    caesar("zz", 1).should == "aa"
  end

  it "handles two words" do
    caesar("hello baby", 3).should == "khoor edeb"
  end

end

